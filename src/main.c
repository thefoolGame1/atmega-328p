#include "vl53l0x.h"
#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

// Konfiguracja peryferium I2C
void i2c_init(void) {
  // Ustawiamy zegar do odczytu na 400kHz (21.5.2)
  TWSR = 0x00; // Preskaler zegara (21.9.3)
  TWBR = 0x0C; // (0x0C = 12)
  // 16_000_000/(16+2*12*4^0) = 400_000

  TWCR = (1 << TWEN); // Włączenie I2C (21.9.2)
}

// Wysyłanie bitu startu
void i2c_start(void) {
  TWCR = (1 << TWSTA) | (1 << TWEN) | (1 << TWINT); // (21.7.1 )

  // Czekanie az preryferium zakonczy prace
  while (!(TWCR & (1 << TWINT))) // (29.9.3)
    ;
}

// Wysłanie bitu stopu
void i2c_stop(void) { TWCR = (1 << TWSTO) | (1 << TWEN) | (1 << TWINT); }

// Wpisanie danych na rejsetr
void i2c_write(uint8_t data) {
  TWDR = data;
  TWCR = (1 << TWEN) | (1 << TWINT);
  while (!(TWCR & (1 << TWINT)))
    ;
}

// Zwrot bitu ACK na magistrale po odczycie
uint8_t i2c_read_ack(void) {
  // Read data with ACK
  TWCR = (1 << TWEN) | (1 << TWINT) | (1 << TWEA);
  while (!(TWCR & (1 << TWINT)))
    ;
  return TWDR;
}

// Odczyt bez bitu ACK
uint8_t i2c_read_nack(void) {
  // Read data with NACK
  TWCR = (1 << TWEN) | (1 << TWINT);
  while (!(TWCR & (1 << TWINT)))
    ;
  return TWDR;
}

void tof_write(uint8_t reg, uint8_t data) {
  i2c_start();
  i2c_write(VL53L0X_I2C_ADDR);
  i2c_write(reg);
  i2c_write(data);
  i2c_stop();
}

uint8_t tof_read(uint8_t reg) {
  uint8_t data;
  i2c_start();
  i2c_write(VL53L0X_I2C_ADDR);
  i2c_write(reg);
  i2c_start();
  i2c_write(VL53L0X_I2C_ADDR | 0x01);
  data = i2c_read_nack();
  i2c_stop();
  return data;
}

void vl53l0x_init(void) {
  // Inicjacja czujnika (moze byc rozwijana)
  tof_write(VL53L0X_REG_SYSRANGE_START, 0x02);
}

uint16_t vl53l0x_read_distance(void) {
  uint8_t distance_high = tof_read(0x1E);
  uint8_t distance_low = tof_read(0x1F);

  uint16_t distance = distance_low + (distance_high << 8);

  if (distance == 20)
    // Naprawa bledu czujnika
    return UINT16_MAX;
  else
    return distance;
}

int main(void) {
  uint16_t distance;

  DDRB |= (1 << PB3);
  DDRB |= (1 << PB4);

  i2c_init();
  vl53l0x_init();

  while (1) {
    PORTB |= (1 << PB4);
    distance = vl53l0x_read_distance();

    if (distance > 100) {
      PORTB |= (1 << PB3);
    } else {
      PORTB &= ~(1 << PB3);
    }

    PORTB &= ~(1 << PB4);
    _delay_ms(100);
  }
  return 0;
}
